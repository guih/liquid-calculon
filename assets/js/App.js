;(function(){
	$ = arguments[0];
	var Liquid = function(args){
		this.options = args;
		this.price;
		this.content;
		this.value;
		this.lock;


		this.main();
	}
	Liquid.prototype.main = function() {
		this.eventHandlers({
			targets: $('input[type=number]'),	
			menuTarget: $('.toggle-menu'),
			_that : this,
		});
	};
	Liquid.prototype.eventHandlers = function(opt){
		var config = opt;
		this.items = config.targets;

		config.targets.on('keyup', function(context, b, c){ this.onKey(context.target);	}.bind(this));

		config.menuTarget.on('click', function(){ this.onToggleMenu(); }.bind(this));

	};
	Liquid.prototype.onKey = function(ctx){
		// On input focused remove error class
		ctx.classList.remove('error');		
		// Filter value replacing NaN value
		ctx.value = ctx.value.replace(/[^0-9]/g, '');	

		// Then validate it
		this.validate(ctx);
	};

	Liquid.prototype.onToggleMenu = function(){
		if( ! $("nav").hasClass('hide_nav') ){
			$("nav").addClass("hide_nav");
			$("body .main-c").addClass("full_content");
			$('#close-menu').hide();
			$('#open-menu').show();			
		}else{
			$("nav").removeClass("hide_nav");
			$("body .main-c").removeClass("full_content");
			$('#close-menu').show();
			$('#open-menu').hide();			
		}
	};
	Liquid.prototype.validate = function(value){
		var item  = value;
		var items = [];

		switch(item.id){
			case 'price':
				if( this.isNaNOrEmpty( item.value ) ){
					this.price = true;
				}else{
					this.price = false;
					item.classList.add('error');
				}				
				break;
			case 'content':
				if( this.isNaNOrEmpty( item.value ) ){
					this.content = true;
				}else{
					this.content = false;
					item.classList.add('error');
				}
				break;
			case 'amount':
				if( this.isNaNOrEmpty( item.value ) ){
					this.amount = true;
				}else{
					this.amount = false;
					item.classList.add('error');
				}	
				break;			
		}
		
		if( 
			this.price == true && 
			this.content == true && 
			this.amount == true 
		){
			setTimeout(function(){
				this.calculate(	this.items, function(data){	this.updateView( data ); }.bind(this) );				
			}.bind(this), 2000);
		}
	}
	Liquid.prototype.calculate = function(items, cb){
		var price   = items[0].value;
		var content = items[1].value;
		var amount  = items[2].value;

		this.lock = false;

		return cb.call(null, {
			defaults:{
				dPrice: price,
				dAmount: amount,
				dContent: content
			},

			perL: ( ( price / content ) * 1000 ).toFixed(2),
			value: ( amount * price ).toFixed(2),
			perMl: ( price / content ).toFixed(4),
			lTotal: ( ( content * amount ) / 1000 ).toFixed(2)
		});
	};
	Liquid.prototype.updateView = function(data, type){
		var unlock = function(data){ this.lock = true; }.bind(this);

		$('.mc-result').text(data.perL+' R$');

		setTimeout(function(){
			this.updateTableView(data, unlock);
		}.bind(this), 2000);
	};
	Liquid.prototype.updateTableView = function(data, cb){
		var nRow;
		if( data.value === $('.mc-store').find('tr').last().find('td').first().text() || $('.mc-store').find('tr').length >= 3 )
			return false;


		nRow = 	[
				'<tr>',
				'<td>'+data.defaults.dPrice+' R$</td>',
				'<td> R$'+data.perMl+' c</td>',
				'<td>'+data.lTotal+'</td>',
				'<td><strong>'+data.value+' R$</strong></td>',
				'</tr>'
			].join("\n");

		if( this.lock === false ){
			$('.mc-store').append(
				nRow
			);

			cb.call(null, data);	 
		}
		
	};

	Liquid.prototype.isNaNOrEmpty = function(val){
		return !isNaN(val) && val != "";
	};

	return new Liquid({a: 1});
}(Zepto));